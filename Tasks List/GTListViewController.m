//
//  GTListViewController.m
//  Tasks List
//
//  Created by Munib Siddiqui on 4/7/15.
//  Copyright (c) 2015 Munib Siddiqui. All rights reserved.
//

#import "GTListViewController.h"
#import "GLTaskDetailViewController.h"

#import "GTMOAuth2ViewControllerTouch.h"

#import "AFNetworkReachabilityManager.h"
#import "AFHTTPRequestOperationManager.h"

#import "TaskCell.h"
#import "Utilities.h"
#import "DejalActivityView.h"
#import "BLLGoogleTask.h"

// Constants that ought to be defined by the API
NSString *const kTaskStatusCompleted = @"completed";
NSString *const kTaskStatusNeedsAction = @"needsAction";

// Keychain item name for saving the user's authentication information
NSString *const kMyClientID = @"1045777117534-98jv7ljudh8b18hlbg3bbofidu7do0a9.apps.googleusercontent.com";
NSString *const kMyClientSecret = @"9nvgZOTayeKaOM3gvvcwgFY_";
NSString *const kKeychainItemName = @"Tasks List: Google Tasks";

@interface GTListViewController () {
    
    AFNetworkReachabilityManager *reachabilityManager;

    NSMutableArray *tasks;
    
    NSInteger taskIndex;
    
    BOOL isUpdate, isError;

    NSString *errorTaskId;
}

@property (nonatomic, retain) GTMOAuth2Authentication *auth;
@property (nonatomic, assign) BOOL isReachable;

@end

@implementation GTListViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    reachabilityManager = [AFNetworkReachabilityManager sharedManager];
    __unsafe_unretained typeof(self) weakSelf = self;
    [reachabilityManager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        NSLog(@"Reachability: %@", AFStringFromNetworkReachabilityStatus(status));
        
        // Check the reachability status and show an alert if the internet connection is not available
        switch (status) {
            case AFNetworkReachabilityStatusUnknown:
                NSLog(@"The reachability status is Unknown");
                weakSelf.isReachable = NO;
                break;
                
            case AFNetworkReachabilityStatusNotReachable:
                NSLog(@"The reachability status is not reachable");
                weakSelf.isReachable = NO;
                break;
                
            case AFNetworkReachabilityStatusReachableViaWWAN:
            case AFNetworkReachabilityStatusReachableViaWiFi:{
                
                NSLog(@"The reachability status is reachable");
                weakSelf.isReachable = YES;
                
                // Get the saved authentication, if any, from the keychain.
                GTMOAuth2Authentication *auth = [GTMOAuth2ViewControllerTouch authForGoogleFromKeychainForName:kKeychainItemName
                                                                                                      clientID:kMyClientID
                                                                                                  clientSecret:kMyClientSecret];
                
                if (auth.canAuthorize) {
                    weakSelf.auth = auth;
                }
                
                break;
            }
                
            default:
                break;
        }
        
        [weakSelf updateUI];
    }];
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:YES];
    
    [reachabilityManager startMonitoring];
}

- (void)viewDidDisappear:(BOOL)animated {
    
    [super viewDidDisappear:YES];
    
    [reachabilityManager stopMonitoring];
}



#pragma mark - Custom Actions

- (NSString *)signedInUsername {
    // Get the email address of the signed-in user
    BOOL isSignedIn = self.auth.canAuthorize;
    if (isSignedIn) {
        return self.auth.userEmail;
    } else {
        return nil;
    }
}

- (BOOL)isSignedIn {
    NSString *name = [self signedInUsername];
    return (name != nil);
}

- (void)updateUI{

    if ([self isSignedIn]) {
        
        [btnSignIn setTitle:@"Sign Out"];
        [btnAddTask setEnabled:YES];
        
        [DejalBezelActivityView activityViewForView:self.view withLabel:@"Please Wait"];
        
        NSArray *tempTasks = [[Utilities fetchTaskByTaskListId:[[NSUserDefaults standardUserDefaults] objectForKey:@"taskListId"]] copy];
        
        for (NSDictionary *task in tempTasks) {
            if (![[(NSManagedObject *)task valueForKey:@"synced"] boolValue]) {
                
                [self performSelectorOnMainThread:@selector(addGoogleTaskListWithObject:) withObject:task waitUntilDone:YES];
            }
        }
        
        [self getGoogleTaskList];
        
    } else {
        [btnSignIn setTitle:@"Sign In"];
        [btnAddTask setEnabled:NO];
        
        tasks = [[Utilities fetchTaskByTaskListId:[[NSUserDefaults standardUserDefaults] objectForKey:@"taskListId"]] mutableCopy];
        
        [tblTaskList reloadData];
    }
}

- (void)viewController:(GTMOAuth2ViewControllerTouch *)viewController
      finishedWithAuth:(GTMOAuth2Authentication *)auth
                 error:(NSError *)error {
    if (error != nil) {
        // Authentication failed
        NSLog(@"Authentication Failed");
    } else {
        // Authentication succeeded
        self.auth = auth;
        
        [[NSUserDefaults standardUserDefaults] setObject:auth.accessToken forKey:@"accessToken"];
        [[NSUserDefaults standardUserDefaults] setObject:auth.refreshToken forKey:@"refreshToken"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    [self updateUI];
}

#pragma mark - RESTful API

- (void)addGoogleTaskListWithObject:(NSDictionary *)object{

    NSString *urlString = [NSString stringWithFormat:@"https://www.googleapis.com/tasks/v1/lists/%@/tasks?access_token=%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"taskListId"],[[NSUserDefaults standardUserDefaults] valueForKey:@"accessToken"]];
    
    NSDictionary *parameters = @{@"title":[object valueForKey:@"title"],
                                 @"notes":[object valueForKey:@"notes"]};
    
   
    NSMutableDictionary *param = [[NSMutableDictionary alloc]init];
    [param setObject:urlString forKey:@"url"];
    [param setObject:parameters forKey:@"param"];
    
    
    [BLLGoogleTask addGoogleTaskListWithObject:param completion:^(id JSON) {
        
        NSLog(@"Task: %@", JSON);
        
        NSMutableDictionary *task = [[[NSMutableDictionary alloc] initWithDictionary:JSON] mutableCopy];
        
        [task setValue:[NSNumber numberWithBool:YES] forKey:@"synced"];
        [task setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"taskListId"] forKey:@"listId"];
        
        if ([Utilities updateTaskWithObject:task withListId:[[NSUserDefaults standardUserDefaults] valueForKey:@"taskListId"]]) {
            NSLog(@"Task Updated");
        }

        
    } failure:^(NSError *error) {
        
        
        NSLog(@"Error: %@", error);
        
        [DejalBezelActivityView removeViewAnimated:YES];
        
        [Utilities showAlertViewWithTitle:@"Error" andMessage:[error localizedDescription]];
        
        
    }];
    

}

- (void)getGoogleTaskList{
    
    NSDictionary *parameters = @{@"access_token":[[NSUserDefaults standardUserDefaults] valueForKey:@"accessToken"]};
    
    
    
    [BLLGoogleTask getGoogleTaskList:parameters completion:^(id JSON) {
        NSLog(@"TaskList: %@", JSON);
        
        [[NSUserDefaults standardUserDefaults] setObject:[[[JSON objectForKey:@"items"] objectAtIndex:0] objectForKey:@"id"] forKey:@"taskListId"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [self setTitle:[[[JSON objectForKey:@"items"] objectAtIndex:0] objectForKey:@"title"]];
        
        [self getGoogleTaskWithTaskListId:[[[JSON objectForKey:@"items"] objectAtIndex:0] objectForKey:@"id"]];
        
        [tblTaskList reloadData];
        

    } failure:^(NSError *error) {
        NSLog(@"Error: %@", error);
        
        [DejalBezelActivityView removeViewAnimated:YES];
        
        [Utilities showAlertViewWithTitle:@"Error" andMessage:[error localizedDescription]];
    }];
    
    
    
}

- (void)getGoogleTaskWithTaskListId:(NSString *)listId{
    
    NSString *urlString = [NSString stringWithFormat:@"https://www.googleapis.com/tasks/v1/lists/%@/tasks", listId];
    
    NSDictionary *parameters = @{@"access_token":[[NSUserDefaults standardUserDefaults] valueForKey:@"accessToken"]};
    
    
    NSMutableDictionary *param = [[NSMutableDictionary alloc]init];
    [param setObject:parameters forKey:@"access_token"];
    [param setObject:urlString forKey:@"url"];
    
    
    
    [BLLGoogleTask getGoogleTaskWithTaskListId:param completion:^(id JSON) {
       
        
        NSLog(@"Tasks: %@", JSON);
        
        for (NSMutableDictionary *item in [[JSON objectForKey:@"items"] mutableCopy]) {
            
            if (![Utilities isTaskExistsWithTaskId:[item objectForKey:@"id"]]) {
                
                NSMutableDictionary *tempTask = [[[NSMutableDictionary alloc] initWithDictionary:item] mutableCopy];
                
                [tempTask setValue:[NSNumber numberWithBool:YES] forKey:@"synced"];
                [tempTask setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"taskListId"] forKey:@"listId"];
                
                if ([Utilities saveTaskWithObject:tempTask]) {
                    NSLog(@"Task Saved");
                }
            }
        }
        
        tasks = [[Utilities fetchTaskByTaskListId:[[NSUserDefaults standardUserDefaults] objectForKey:@"taskListId"]] mutableCopy];
        
        [tblTaskList reloadData];
        
        [DejalBezelActivityView removeViewAnimated:YES];

    } failure:^(NSError *error) {
        
        
        NSLog(@"Error: %@", error);
        
        [DejalBezelActivityView removeViewAnimated:YES];
        
        [Utilities showAlertViewWithTitle:@"Error" andMessage:[error localizedDescription]];
        
    }];
    
    
   
}

#pragma mark - IBActions

- (IBAction)onSignInClicked:(id)sender {
    
    if (![[btnSignIn title] isEqualToString:@"Sign Out"]) {
        // Sign in
        GTMOAuth2ViewControllerTouch *viewController;
        viewController = [[GTMOAuth2ViewControllerTouch alloc] initWithScope:kGTLAuthScopeTasks
                                                                    clientID:kMyClientID
                                                                clientSecret:kMyClientSecret
                                                            keychainItemName:kKeychainItemName
                                                                    delegate:self
                                                            finishedSelector:@selector(viewController:finishedWithAuth:error:)];
        
        [[self navigationController] pushViewController:viewController animated:YES];
    } else {
        // Sign out
        [GTMOAuth2ViewControllerTouch removeAuthFromKeychainForName:kKeychainItemName];

        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"accessToken"];
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"refreshToken"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        self.auth = nil;
        
        [self updateUI];
    }
}

-(void)onTaskDeleteClicked:(id)sender
{
    UIButton *senderButton = (UIButton *)sender;
    
    for(NSManagedObject *task in tasks) {
        
        if([[task valueForKey:@"id"] isEqualToString:[[tasks objectAtIndex:senderButton.tag] valueForKey:@"id"]] && self.isReachable) {
        
            if ([self isSignedIn]) {
                
                [DejalBezelActivityView activityViewForView:self.view withLabel:@"Please Wait"];
                
                NSString *urlString = [NSString stringWithFormat:@"https://www.googleapis.com/tasks/v1/lists/%@/tasks/%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"taskListId"], [task valueForKey:@"id"]];
                
                NSDictionary *parameters = @{@"access_token":[[NSUserDefaults standardUserDefaults] valueForKey:@"accessToken"]};
                
                NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
                [params setObject:urlString forKey:@"url"];
                [params setObject:parameters forKey:@"param"];
               
                [BLLGoogleTask deleteGoogleTaskListWithObject:params completion:^(id JSON) {
                   
                    NSLog(@"Deleted");
                    
                    if ([Utilities deleteTaskWithObject:task]) {
                        
                        [tasks removeObject:task];
                        
                        [tblTaskList reloadData];
                        
                        [DejalBezelActivityView removeViewAnimated:YES];
                    }

                
                } failure:^(NSError *error) {
                    
                    NSLog(@"Error: %@", error);
                    
                    isError = YES;
                    
                    errorTaskId = [[tasks objectAtIndex:senderButton.tag] valueForKey:@"id"];
                    
                    [DejalBezelActivityView removeViewAnimated:YES];
                    
                }];
            
            
            } else {
                
                [Utilities showAlertViewWithTitle:@"Alert" andMessage:@"You need to sign in first"];
            }
        }
    }
}

#pragma mark - UITableView DataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [tasks count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    TaskCell *cell = (TaskCell *)[tableView dequeueReusableCellWithIdentifier:@"TaskCell"];
    
    NSManagedObject *task = (NSManagedObject *)[tasks objectAtIndex:indexPath.row];
    
    if ([[task valueForKey:@"synced"] boolValue] && self.isReachable && [self isSignedIn]) {
        [[cell contentView] setBackgroundColor:[UIColor colorWithRed:121.0/255.0 green:199.0/255.0 blue:103.0/255.0 alpha:1]];
        

    } else if(isError && [[task valueForKey:@"id"] isEqualToString:errorTaskId]){
        [[cell contentView] setBackgroundColor:[UIColor redColor]];
    }else {
        [[cell contentView] setBackgroundColor:[UIColor orangeColor]];
    }
    
    if ([[task valueForKey:@"title"] length] > 0) {
        [[cell lblTitle] setText:[task valueForKey:@"title"]];
    } else {
        [[cell lblTitle] setText:@"No Title"];
    }
    
    [[cell btnDelete] setTag:indexPath.row];
    [[cell btnDelete] addTarget:self action:@selector(onTaskDeleteClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

#pragma mark - UITableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    isUpdate = YES;
    taskIndex = indexPath.row;
    
    [self performSegueWithIdentifier:@"segueUpdateTask" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if([[segue identifier] isEqualToString:@"segueUpdateTask"] && isUpdate) {
        
        GLTaskDetailViewController *viewController = (GLTaskDetailViewController *) [segue destinationViewController];
        viewController.isUpdate = isUpdate;
        viewController.taskId = [[tasks objectAtIndex:taskIndex] valueForKey:@"id"];
        viewController.taskTitle = [[tasks objectAtIndex:taskIndex] valueForKey:@"title"];
        
        if ([[tasks objectAtIndex:taskIndex] valueForKey:@"notes"]) {
            viewController.taskDescription = [[tasks objectAtIndex:taskIndex] valueForKey:@"notes"];
        }
    }
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
