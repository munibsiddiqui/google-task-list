//
//  TaskCell.m
//  Tasks List
//
//  Created by Munib Siddiqui on 4/7/15.
//  Copyright (c) 2015 Munib Siddiqui. All rights reserved.
//

#import "TaskCell.h"

@implementation TaskCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
