//
//  GLTaskDetailViewController.h
//  Tasks Detail
//
//  Created by Munib Siddiqui on 4/7/15.
//  Copyright (c) 2015 Munib Siddiqui. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface GLTaskDetailViewController : UIViewController <CLLocationManagerDelegate>{
    
    IBOutlet UITextField *txtTitle;
    IBOutlet UITextView *txtDescription;
}

@property (nonatomic, retain) NSString *taskId;
@property (nonatomic, retain) NSString *taskTitle;
@property (nonatomic, retain) NSString *taskDescription;

@property (assign) BOOL isUpdate;

@end
