//
//  BLLGoogleTask.m
//  Tasks List
//
//  Created by Munib Siddiqui on 4/7/15.
//  Copyright (c) 2015 Munib Siddiqui. All rights reserved.
//

#import "BLLGoogleTask.h"
#import "AFNetworkReachabilityManager.h"
#import "AFHTTPRequestOperationManager.h"

@implementation BLLGoogleTask

+(void)getGoogleTaskList:(NSDictionary *)params
                                     completion:(void (^)(id JSON))completion
                                        failure:(void (^)(NSError * error))failure{
    

    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"application/json"];
    manager.securityPolicy.allowInvalidCertificates = YES;
    
    [manager GET:@"https://www.googleapis.com/tasks/v1/users/@me/lists"
      parameters:params
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             
             completion(responseObject);
             
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             
             
             failure(error);
         }];
         
         
         
    
    
}

+(void)getGoogleTaskWithTaskListId:(NSMutableDictionary *)params
                                  completion:(void (^)(id JSON))completion
                                     failure:(void (^)(NSError * error))failure{

    
    
     NSString *urlString = [params objectForKey:@"url"];
    [params removeObjectForKey:@"url"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"application/json"];
    manager.securityPolicy.allowInvalidCertificates = YES;
    
    [manager GET:urlString
      parameters:[params objectForKey:@"access_token"]
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             
             completion(responseObject);
             
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             
             failure(error);
         }
     ];

    
}



+(void)addGoogleTaskListWithObject:(NSMutableDictionary *)params
                        completion:(void (^)(id JSON))completion
                           failure:(void (^)(NSError * error))failure{

    
    NSString *urlString = [params objectForKey:@"url"];
    [params removeObjectForKey:@"url"];
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"application/json"];
    manager.securityPolicy.allowInvalidCertificates = YES;
    
    [manager POST:urlString
       parameters:[params objectForKey:@"param"]
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              
              completion(responseObject);
              
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              
              failure(error);
          }
     ];
}


+(void)deleteGoogleTaskListWithObject:(NSMutableDictionary *)params
                        completion:(void (^)(id JSON))completion
                           failure:(void (^)(NSError * error))failure{

    NSString *urlString = [params objectForKey:@"url"];
    [params removeObjectForKey:@"url"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"application/json"];
    manager.securityPolicy.allowInvalidCertificates = YES;
    
    [manager DELETE:urlString
         parameters:[params objectForKey:@"param"]
            success:^(AFHTTPRequestOperation *operation, id responseObject) {
                
                completion(responseObject);
                               
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {

                failure(error);
            }
     ];

}
@end



