//
//  GTListViewController.h
//  Tasks List
//
//  Created by Munib Siddiqui on 4/7/15.
//  Copyright (c) 2015 Munib Siddiqui. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GTListViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>{
    
    IBOutlet UIBarButtonItem *btnSignIn, *btnAddTask;
    
    IBOutlet UITableView *tblTaskList;
}

@end

