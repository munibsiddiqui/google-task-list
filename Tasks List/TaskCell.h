//
//  TaskCell.h
//  Tasks List
//
//  Created by Munib Siddiqui on 4/7/15.
//  Copyright (c) 2015 Munib Siddiqui. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TaskCell : UITableViewCell

@property (nonatomic,retain) IBOutlet UILabel *lblTitle;
@property (nonatomic,retain) IBOutlet UIButton *btnDelete;

@end
