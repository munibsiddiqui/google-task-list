//
//  BLLGoogleTask.h
//  Tasks List
//
//  Created by Munib Siddiqui on 4/7/15.
//  Copyright (c) 2015 Munib Siddiqui. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BLLGoogleTask : NSObject

// get task list
+(void)getGoogleTaskList:(NSDictionary *)params
                                     completion:(void (^)(id JSON))completion
                                        failure:(void (^)(NSError * error))failure;

//get task with id
+(void)getGoogleTaskWithTaskListId:(NSMutableDictionary *)params
                                        completion:(void (^)(id JSON))completion
                                        failure:(void (^)(NSError * error))failure;



+(void)addGoogleTaskListWithObject:(NSMutableDictionary *)params
                                    completion:(void (^)(id JSON))completion
                                       failure:(void (^)(NSError * error))failure;


+(void)deleteGoogleTaskListWithObject:(NSMutableDictionary *)params
                        completion:(void (^)(id JSON))completion
                           failure:(void (^)(NSError * error))failure;
@end
